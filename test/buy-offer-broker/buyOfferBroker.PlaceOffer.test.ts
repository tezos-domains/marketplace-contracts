import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { assert } from 'chai';
import { buyOfferBrokerContract } from '../../common/contracts/buyOfferBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { itShouldFail } from '../utils';
import { BuyOfferBroker, BuyOfferBrokerStorage, offerKey } from './common';


describe('BuyOfferBroker.PlaceOffer', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const tokenContractAddress = 'KT1VmuKWcqSJ35RwPigAhLvu43Y11jED37fy';

    let contract: Contract;
    let tezos: TezosToolkit;
    let store: BuyOfferBrokerStorage;

    before(async () => {
        tezos = tezosToolkit();

        const offers = new MichelsonMap();
        offers.set(offerKey(bob.pkh, tokenContractAddress, 1), { fee: new BigNumber('25000'), price: new BigNumber('1000000'), expiration: undefined });

        const deployed = await buyOfferBrokerContract.deploy(
            {},
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<BuyOfferBrokerStorage>();
    });

    async function placeOffer(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .place_offer(
                    options.tokenContract || 'KT1VmuKWcqSJ35RwPigAhLvu43Y11jED37fy',
                    options.tokenId || 1,
                    options.price || 1e7,
                    options.expiration || new Date(5555555555000)
                )
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should create an offer', async () => {
        await placeOffer({ price: 2_000_000, amount: 2_050_000 });

        const offer = await store.offers.get<BuyOfferBroker>(offerKey(alice.pkh, tokenContractAddress, 1));

        assert.equal(offer!.price.toNumber(), 2_000_000);
        assert.equal(offer!.fee.toNumber(), 50_000);
        assert.equal(Date.parse(offer!.expiration), 5555555555000);
    });

    itShouldFail(async () => {
        await placeOffer({ price: 2_000_000, amount: 2_050_000, tokenId: 2 });
        await placeOffer({ price: 2_000_000, amount: 2_050_000, tokenId: 2 });

        await placeOffer();
    }, 'DUPLICATE_OFFER');

    itShouldFail(placeOffer, 'PRICE_TOO_LOW', { price: 900000 });
    itShouldFail(placeOffer, 'AMOUNT_TOO_LOW', { price: 2_000_000, amount: 2_049_999 });
    itShouldFail(placeOffer, 'AMOUNT_TOO_HIGH', { price: 2_000_000, amount: 2_050_001 });
    itShouldFail(placeOffer, 'EXPIRATION_PAST', { price: 2_000_000, amount: 2_050_000, tokenId: 3, expiration: new Date(1627393581000) });
    itShouldFail(async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        await runOperation(async () => await contract.methods.update_config(true, 0, 0, alice.pkh, alice.pkh, tokenContractAddress).send());

        await placeOffer();
    }, 'CONTRACT_PAUSED');
});
