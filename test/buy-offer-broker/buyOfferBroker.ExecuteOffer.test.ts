import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { assert } from 'chai';
import { compileContract } from '../../common/compile';
import { buyOfferBrokerContract } from '../../common/contracts/buyOfferBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { TransferParam } from '../transfer-param';
import { itShouldFail } from '../utils';
import { BuyOfferBrokerStorage, offerKey } from './common';

describe('BuyOfferBroker.ExecuteOffer', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;

    let contract: Contract;
    let tokenContractMock: Contract;
    let tezos: TezosToolkit;
    let store: BuyOfferBrokerStorage;

    before(async () => {
        tezos = tezosToolkit();

        const op = await runOperation(async () => tezos.contract.originate({
            code: await compileContract('test/Tzip12ContractMock.mligo'),
            storage: null,
        }));

        tokenContractMock = await tezos.contract.at(op.contractAddress!);

        const offers = new MichelsonMap();
        offers.set(offerKey(alice.pkh, tokenContractMock.address, 1), { price: new BigNumber('1000000'), fee: new BigNumber('25000'), expiration: undefined });
        offers.set(offerKey(alice.pkh, tokenContractMock.address, 2), { price: new BigNumber('1000000'), fee: new BigNumber('25000'), expiration: new Date(2020, 1, 1) });
        offers.set(offerKey(alice.pkh, tokenContractMock.address, 3), { price: new BigNumber('1000000'), fee: new BigNumber('25000'), expiration: new Date(2150, 1, 1) });
        offers.set(offerKey(alice.pkh, 'KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p', 4), { price: new BigNumber('1000000'), fee: new BigNumber('25000'), expiration: undefined });

        const deployed = await buyOfferBrokerContract.deploy(
            {
                proceedRecipient: eve.pkh,
                tokenContract: tokenContractMock.address,
                balance: 10
            },
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<BuyOfferBrokerStorage>();
    });

    async function executeOffer(options: { tokenContract?: string, tokenId?: number, buyer?: string, funds_recipient?: string, sender?: any, amount?: number } = {}) {
        const sender = options.sender || bob;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .execute_offer(
                    options.tokenContract || tokenContractMock.address,
                    options.tokenId || 1,
                    options.buyer ?? alice.pkh,
                    options.funds_recipient ?? alice.pkh)
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should execute an offer', async () => {
        const aliceBalance = await tezos.rpc.getBalance(alice.pkh);
        const bobBalance = await tezos.rpc.getBalance(bob.pkh);
        const eveBalance = await tezos.rpc.getBalance(eve.pkh);
        const contractBalance = await tezos.rpc.getBalance(contract.address);

        await executeOffer({ tokenId: 1, buyer: alice.pkh, funds_recipient: bob.pkh });

        const offer = await store.offers.get(offerKey(alice.pkh, tokenContractMock.address, 1));

        assert.isUndefined(offer);

        const newAliceBalance = await tezos.rpc.getBalance(alice.pkh);
        const newBobBalance = await tezos.rpc.getBalance(bob.pkh);
        const newEveBalance = await tezos.rpc.getBalance(eve.pkh);
        const newContractBalance = await tezos.rpc.getBalance(contract.address);

        // no extra cost for offer creator (apart for already escrowed) 
        assert.equal(newAliceBalance.minus(aliceBalance).toNumber(), 0);

        // bob is the funds_recipient so they get the price for the domain minus the gas(20_226) since they also executed the offer
        assert.approximately(newBobBalance.minus(bobBalance).toNumber(), 1_000_000 - 20_226, 5000);

        // fee should have been transferred to account specified in the contract config
        assert.equal(newEveBalance.minus(eveBalance).toNumber(), 25_000);

        // contract should transfer the price + the fee
        assert.equal(newContractBalance.minus(contractBalance).toNumber(), -1_025_000);

        const transfers = await tokenContractMock.storage<TransferParam[]>();

        assert.equal(transfers.length, 1);

        const transfer = transfers[0];

        assert.equal(transfer.from_, bob.pkh);
        assert.equal(transfer.txs.length, 1);
        assert.equal(transfer.txs[0].to_, alice.pkh);
        assert.equal(transfer.txs[0].token_id, 1);
        assert.equal(transfer.txs[0].amount, 1);
    });

    itShouldFail(executeOffer, 'OFFER_NOT_FOUND', { tokenId: 100 });
    itShouldFail(executeOffer, 'OFFER_EXPIRED', { tokenId: 2 });
    itShouldFail(executeOffer, 'AMOUNT_NOT_ZERO', { tokenId: 1, amount: 1_000_000 });
    itShouldFail(executeOffer, 'INVALID_TOKEN_CONTRACT', { tokenId: 4, tokenContract: 'KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p' });

    itShouldFail(async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        await runOperation(async () => await contract.methods.update_config(true, 0, 0, alice.pkh, alice.pkh).send());

        await executeOffer();
    }, 'CONTRACT_PAUSED');
});
