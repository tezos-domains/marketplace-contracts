import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import { assert } from 'chai';
import { buyOfferBrokerContract } from '../../common/contracts/buyOfferBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { itShouldFail } from '../utils';
import { BuyOfferBrokerStorage } from './common';

describe('BuyOfferBroker.UpdateConfig', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    const tokenContractAddress = 'KT1VmuKWcqSJ35RwPigAhLvu43Y11jED37fy';

    let contract: Contract;
    let tezos: TezosToolkit;
    let store: BuyOfferBrokerStorage;

    before(async () => {
        tezos = tezosToolkit();

        const offers = new MichelsonMap();
        const deployed = await buyOfferBrokerContract.deploy(
            {},
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<BuyOfferBrokerStorage>();
    });

    async function updateConfig(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .update_config(
                    options.pause || false,
                    options.fee || 25000,
                    options.minPrice || 1e6,
                    options.proceedRecipient || alice.pkh,
                    options.owner || alice.pkh
                )
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should update config', async () => {
        await updateConfig({ pause: true, fee: 10000, minPrice: 1e7, proceedRecipient: bob.pkh, owner: eve.pkh, token_contract: tokenContractAddress });

        const config = (await contract.storage<BuyOfferBrokerStorage>()).config;

        assert.isTrue(config.pause);
        assert.equal(config.fee_ratio.toNumber(), 10000);
        assert.equal(config.min_price.toNumber(), 1e7);
        assert.equal(config.proceed_recipient, bob.pkh);
        assert.equal(config.owner, eve.pkh);
    });

    itShouldFail(updateConfig, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(updateConfig, 'NOT_AUTHORIZED', { sender: bob });
});
