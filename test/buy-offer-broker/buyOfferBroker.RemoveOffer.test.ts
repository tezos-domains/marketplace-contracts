import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { assert } from 'chai';
import { buyOfferBrokerContract } from '../../common/contracts/buyOfferBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { itShouldFail } from '../utils';
import { BuyOfferBrokerStorage, offerKey } from './common';

describe('BuyOfferBroker.RemoveOffer', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const tokenContractAddress = 'KT1VmuKWcqSJ35RwPigAhLvu43Y11jED37fy';
    let contract: Contract;
    let tezos: TezosToolkit;
    let store: BuyOfferBrokerStorage;


    before(async () => {
        tezos = tezosToolkit();

        const offers = new MichelsonMap();
        offers.set(offerKey(alice.pkh, tokenContractAddress, 1), { price: new BigNumber('2000000'), fee: new BigNumber('50000'), expiration: undefined });

        const deployed = await buyOfferBrokerContract.deploy(
            {
                balance: 20
            },
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<BuyOfferBrokerStorage>();
    });

    async function removeOffer(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .remove_offer(
                    options.tokenContract || 'KT1VmuKWcqSJ35RwPigAhLvu43Y11jED37fy',
                    options.tokenId || 1,
                    options.recipient || accounts.bob.pkh
                )
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should remove an offer', async () => {
        const key = offerKey(alice.pkh, tokenContractAddress, 1);

        assert.isNotNull(await store.offers.get(key));
        const contractBalance = await tezos.rpc.getBalance(contract.address);
        const bobBalance = await tezos.rpc.getBalance(bob.pkh);

        await removeOffer();

        const newContractBalance = await tezos.rpc.getBalance(contract.address);
        const newBobBalance = await tezos.rpc.getBalance(bob.pkh);

        const offer = await store.offers.get(key);

        assert.equal(newContractBalance.minus(contractBalance).toNumber(), -2_050_000);
        assert.equal(newBobBalance.minus(bobBalance).toNumber(), 2_050_000);
        assert.isUndefined(offer);
    });

    itShouldFail(removeOffer, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(removeOffer, 'OFFER_NOT_FOUND', { tokenId: 100 });
    itShouldFail(async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        await runOperation(async () => await contract.methods.update_config(true, 0, 0, alice.pkh, alice.pkh, tokenContractAddress).send());

        await removeOffer();
    }, 'CONTRACT_PAUSED');
});
