export interface TransferTx {
    to_: string;
    token_id: number;
    amount: number;
}

export interface TransferParam {
    from_: string;
    txs: TransferTx[]
}