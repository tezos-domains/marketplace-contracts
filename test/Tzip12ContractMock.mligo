#include "../src/contracts/transfer-param.mligo"

type parameter =
    Transfer of tzip12_transfer_param
    | Void of unit

type storage = tzip12_transfer_param option

type return = operation list * storage

let main (action, store : parameter * storage) : return =
    match action with
        | Transfer p -> ([] : operation list), Some p
        | Void _ -> ([] : operation list), store
