import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { assert } from 'chai';
import { directBrokerContract } from '../../common/contracts/directBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { itShouldFail } from '../utils';
import { DirectBrokerStorage, offerKey } from './common';

describe('DirectBroker.UpdateConfig', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    const tokenContractAddress = 'KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p';
    let contract: Contract;
    let tezos: TezosToolkit;
    let store: DirectBrokerStorage;

    before(async () => {
        tezos = tezosToolkit();

        const offers = new MichelsonMap();
        offers.set(offerKey(alice.pkh, tokenContractAddress, 1), { price: new BigNumber('1000000'), expiration: undefined });

        const deployed = await directBrokerContract.deploy(
            {},
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<DirectBrokerStorage>();
    });

    async function updateConfig(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .update_config(
                    options.pause || false,
                    options.fee || 25000,
                    options.minPrice || 1e6,
                    options.proceedRecipient || alice.pkh,
                    options.owner || alice.pkh
                )
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should update config', async () => {
        await updateConfig({ pause: true, fee: 10000, minPrice: 1e7, proceedRecipient: bob.pkh, owner: eve.pkh });

        const config = (await contract.storage<DirectBrokerStorage>()).config;

        assert.isTrue(config.pause);
        assert.equal(config.fee_ratio.toNumber(), 10000);
        assert.equal(config.min_price.toNumber(), 1e7);
        assert.equal(config.proceed_recipient, bob.pkh);
        assert.equal(config.owner, eve.pkh);
    });

    itShouldFail(updateConfig, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(updateConfig, 'NOT_AUTHORIZED', { sender: bob });
});
