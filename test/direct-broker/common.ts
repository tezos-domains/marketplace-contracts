import { BigMapAbstraction } from '@taquito/taquito';
import BigNumber from 'bignumber.js';

export interface DirectBrokerStorage {
    offers: BigMapAbstraction;
    config: any;
}

export interface DirectBrokerOffer {
    price: BigNumber;
    expiration: string;
}

export function offerKey(seller: string, tokenContract: string, tokenId: number) {
    return { 0: seller, 1: tokenContract, 2: tokenId };
}
