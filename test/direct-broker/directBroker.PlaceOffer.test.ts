import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { assert } from 'chai';
import { directBrokerContract } from '../../common/contracts/directBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { itShouldFail } from '../utils';
import { DirectBrokerOffer, DirectBrokerStorage, offerKey } from './common';

describe('DirectBroker.PlaceOffer', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const tokenContractAddress = 'KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p';
    let contract: Contract;
    let tezos: TezosToolkit;
    let store: DirectBrokerStorage;

    before(async () => {
        tezos = tezosToolkit();

        const offers = new MichelsonMap();
        offers.set(offerKey(bob.pkh, tokenContractAddress, 1), { price: new BigNumber('1000000'), expiration: undefined });

        const deployed = await directBrokerContract.deploy(
            {},
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<DirectBrokerStorage>();
    });

    async function placeOffer(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .place_offer(
                    options.tokenContract || tokenContractAddress,
                    options.tokenId || 1,
                    options.price || 1e7,
                    options.expiration || new Date(5555555555000)
                )
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should create an offer', async () => {
        await placeOffer();

        const offer = await store.offers.get<DirectBrokerOffer>(offerKey(alice.pkh, tokenContractAddress, 1));

        assert.equal(offer!.price.toNumber(), 1e7);
        assert.equal(Date.parse(offer!.expiration), 5555555555000);
    });

    it('should update an offer', async () => {
        const key = offerKey(bob.pkh, tokenContractAddress, 1);

        assert.isNotNull(await store.offers.get(key));

        await placeOffer({ sender: bob, price: 15e6, expiration: new Date(6666666666000) });

        const offer = await store.offers.get<DirectBrokerOffer>(key);

        assert.equal(offer!.price.toNumber(), 15e6);
        assert.equal(Date.parse(offer!.expiration), 6666666666000);
    });

    itShouldFail(placeOffer, 'PRICE_TOO_LOW', { price: 900000 });
    itShouldFail(placeOffer, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(placeOffer, 'EXPIRATION_PAST', { expiration: new Date(1627393581000) });
    itShouldFail(async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        await runOperation(async () => await contract.methods.update_config(true, 0, 0, alice.pkh, alice.pkh).send());

        await placeOffer();
    }, 'CONTRACT_PAUSED');
});
