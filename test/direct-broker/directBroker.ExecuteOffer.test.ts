import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { assert } from 'chai';
import { compileContract } from '../../common/compile';
import { directBrokerContract } from '../../common/contracts/directBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { TransferParam } from '../transfer-param';
import { itShouldFail } from '../utils';
import { DirectBrokerStorage, offerKey } from './common';

describe('DirectBroker.ExecuteOffer', () => {
    const alice = accounts.alice;
    const bob = accounts.bob;
    const eve = accounts.eve;
    let contract: Contract;
    let tokenContractMock: Contract;
    let tezos: TezosToolkit;
    let store: DirectBrokerStorage;

    before(async () => {
        tezos = tezosToolkit();

        const op = await runOperation(async () => tezos.contract.originate({
            code: await compileContract('test/Tzip12ContractMock.mligo'),
            storage: null,
        }));

        tokenContractMock = await tezos.contract.at(op.contractAddress!);

        const offers = new MichelsonMap();
        offers.set(offerKey(alice.pkh, tokenContractMock.address, 1), { price: new BigNumber('10000000'), expiration: undefined });
        offers.set(offerKey(alice.pkh, tokenContractMock.address, 2), { price: new BigNumber('10000000'), expiration: new Date(2020, 1, 1) });
        offers.set(offerKey(alice.pkh, tokenContractMock.address, 3), { price: new BigNumber('10000000'), expiration: new Date(2150, 1, 1) });
        offers.set(offerKey(alice.pkh, 'KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p', 1), { price: new BigNumber('10000000'), expiration: undefined });

        const deployed = await directBrokerContract.deploy(
            {
                proceedRecipient: eve.pkh,
            },
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<DirectBrokerStorage>();
    });

    async function executeOffer(options: any = {}) {
        const sender = options.sender || bob;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .execute_offer(options.tokenContract || tokenContractMock.address, options.tokenId || 1, options.seller || alice.pkh)
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should execute an offer', async () => {
        const aliceBalance = await tezos.rpc.getBalance(alice.pkh);
        const bobBalance = await tezos.rpc.getBalance(bob.pkh);
        const eveBalance = await tezos.rpc.getBalance(eve.pkh);

        await executeOffer({ amount: 1e7 });

        const offer = await store.offers.get(offerKey(alice.pkh, tokenContractMock.address, 1));

        assert.isUndefined(offer);

        const newAliceBalance = await tezos.rpc.getBalance(alice.pkh);
        const newBobBalance = await tezos.rpc.getBalance(bob.pkh);
        const newEveBalance = await tezos.rpc.getBalance(eve.pkh);

        assert.equal(newAliceBalance.minus(aliceBalance).toNumber(), 9750000);
        assert.approximately(newBobBalance.minus(bobBalance).toNumber(), -10020861, 5000); // 20861 - gas/storage fee
        assert.equal(newEveBalance.minus(eveBalance).toNumber(), 250000);

        const transfers = await tokenContractMock.storage<TransferParam[]>();

        assert.equal(transfers.length, 1);

        const transfer = transfers[0];

        assert.equal(transfer.from_, alice.pkh);
        assert.equal(transfer.txs.length, 1);
        assert.equal(transfer.txs[0].to_, bob.pkh);
        assert.equal(transfer.txs[0].token_id, 1);
        assert.equal(transfer.txs[0].amount, 1);
    });

    itShouldFail(executeOffer, 'OFFER_NOT_FOUND', { tokenId: 100 });
    itShouldFail(executeOffer, 'OFFER_EXPIRED', { tokenId: 2 });
    itShouldFail(executeOffer, 'AMOUNT_TOO_LOW', { amount: 1e7 - 1, tokenId: 3 });
    itShouldFail(executeOffer, 'AMOUNT_TOO_HIGH', { amount: 1e7 + 1, tokenId: 3 });
    itShouldFail(executeOffer, 'INVALID_TOKEN_CONTRACT', { tokenContract: 'KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p', amount: 1e7 });
    itShouldFail(async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        await runOperation(async () => await contract.methods.update_config(true, 0, 0, alice.pkh, alice.pkh).send());

        await executeOffer();
    }, 'CONTRACT_PAUSED');
});
