import { InMemorySigner } from '@taquito/signer';
import { Contract, MichelsonMap, TezosToolkit } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { assert } from 'chai';
import { directBrokerContract } from '../../common/contracts/directBroker.contract';
import accounts from '../../common/sandbox-accounts';
import { runOperation, tezosToolkit } from '../../common/tezos-toolkit';
import { itShouldFail } from '../utils';
import { DirectBrokerStorage, offerKey } from './common';

describe('DirectBroker.RemoveOffer', () => {
    const alice = accounts.alice;
    const tokenContractAddress = 'KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p';
    let contract: Contract;
    let tezos: TezosToolkit;
    let store: DirectBrokerStorage;

    before(async () => {
        tezos = tezosToolkit();

        const offers = new MichelsonMap();
        offers.set(offerKey(alice.pkh, tokenContractAddress, 1), { price: new BigNumber('1000000'), expiration: undefined });

        const deployed = await directBrokerContract.deploy(
            {},
            {
                offers,
            }
        );

        contract = await tezos.contract.at(deployed.address);
        store = await contract.storage<DirectBrokerStorage>();
    });

    async function removeOffer(options: any = {}) {
        const sender = options.sender || alice;
        tezos.setSignerProvider(new InMemorySigner(sender.sk));

        await runOperation(() =>
            contract.methods
                .remove_offer(
                    options.tokenContract || tokenContractAddress,
                    options.tokenId || 1,
                )
                .send({ amount: options.amount, mutez: true })
        );
    }

    it('should remove an offer', async () => {
        const key = offerKey(alice.pkh, tokenContractAddress, 1);

        assert.isNotNull(await store.offers.get(key));

        await removeOffer();

        const offer = await store.offers.get(key);

        assert.isUndefined(offer);
    });

    itShouldFail(removeOffer, 'AMOUNT_NOT_ZERO', { amount: 123 });
    itShouldFail(removeOffer, 'OFFER_NOT_FOUND', { tokenId: 100 });
    itShouldFail(async () => {
        tezos.setSignerProvider(new InMemorySigner(alice.sk));
        await runOperation(async () => await contract.methods.update_config(true, 0, 0, alice.pkh, alice.pkh).send());

        await removeOffer();
    }, 'CONTRACT_PAUSED');
});
