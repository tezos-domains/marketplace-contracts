
type tzip12_transfer_destination =
[@layout:comb]
{
  to_: address;
  token_id: nat;
  amount: nat;
}

type tzip12_transfer =
[@layout:comb]
{
  from_: address;
  txs: tzip12_transfer_destination list;
}

type tzip12_transfer_param = tzip12_transfer list