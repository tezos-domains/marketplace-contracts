type place_offer_param = [@layout:comb] {
    token_contract: address;
    token_id: nat;
    price: tez;
    expiration: timestamp option;
}

type remove_offer_param = [@layout:comb] {
    token_contract: address;
    token_id: nat;
    recipient: address;
}

type execute_offer_param = [@layout:comb] {
    token_contract: address;
    token_id: nat;
    buyer: address;
    funds_recipient: address;
}