#include "../transfer-param.mligo"
#include "./param.mligo"

type offer = [@layout:comb] {
    price: tez;
    fee: tez;
    expiration: timestamp option;
}

// buyer * token_contract * token_id
type offer_key = address * address * nat

type config = [@layout:comb] {
    pause: bool;
    fee_ratio: nat;
    min_price: tez;
    proceed_recipient: address;
    owner: address;
}

type storage = [@layout:comb] {
    offers: (offer_key, offer) big_map;
    config: config;
}

type return = operation list * storage


[@inline]
let require_not_paused (store: storage) : unit =
    if store.config.pause then (failwith "CONTRACT_PAUSED": unit)

[@inline]
let require_zero_amount (_ : unit) : unit = 
    if Tezos.get_amount() > 0tez then (failwith "AMOUNT_NOT_ZERO" : unit)

[@inline]
let require_min_price (price, store: tez * storage) : unit =
    if price < store.config.min_price then (failwith "PRICE_TOO_LOW" : unit)

[@inline]
let require_correct_amount (amount, price, fee: tez * tez * tez) : unit =
    let required_amount = price + fee in

    if amount < required_amount then (failwith "AMOUNT_TOO_LOW" : unit)
    else 
        if amount > required_amount then (failwith "AMOUNT_TOO_HIGH" : unit)

let place_offer (param, store : place_offer_param * storage): return =
    let assert_not_paused = require_not_paused(store) in
    let assert_min_price = require_min_price(param.price, store) in
    let fee = (param.price * store.config.fee_ratio) / 1_000_000n in
    let assert_total_amount = require_correct_amount(Tezos.get_amount(), param.price, fee) in
    
    let key: offer_key = (Tezos.get_sender(), param.token_contract, param.token_id) in

    let assert_no_offer = match Big_map.find_opt key store.offers with
        Some o -> (failwith "DUPLICATE_OFFER": unit)
        | None -> () in
    
    let assert_expiration = match param.expiration with
        Some ts -> if ts <= Tezos.get_now() then (failwith "EXPIRATION_PAST": unit)
        | None -> () in

    let offer: offer = {
        price = param.price;
        fee = fee;
        expiration = param.expiration;
    } in

    ([] : operation list), { store with offers = Big_map.update key (Some offer) store.offers }

let remove_offer(param, store: remove_offer_param * storage): return =
    let assert_not_paused = require_not_paused(store) in
    let assert_zero_amount = require_zero_amount () in
    let key: offer_key = (Tezos.get_sender(), param.token_contract, param.token_id) in
    
    let offer = match Big_map.find_opt key store.offers with
        Some o -> o
        | None -> (failwith "OFFER_NOT_FOUND": offer) in

    let total_escrow = offer.price + offer.fee in
    let recipient = (Tezos.get_contract_with_error param.recipient "INVALID_SENDER" : unit contract) in
    let return_escrow = Tezos.transaction () total_escrow recipient in

    ([return_escrow] : operation list), { store with offers = Big_map.remove key store.offers }

let execute_offer (param, store : execute_offer_param * storage) : return =
    let assert_not_paused = require_not_paused(store) in
    let assert_zero_amount = require_zero_amount() in
    let key: offer_key = (param.buyer, param.token_contract, param.token_id) in

    let offer = match Big_map.find_opt key store.offers with
        Some o -> o
        | None -> (failwith "OFFER_NOT_FOUND": offer) in

    let assert_expiration = match offer.expiration with
        Some ts -> if ts <= Tezos.get_now() then (failwith "OFFER_EXPIRED": unit)
        | None -> () in

    let payment_transaction = match (Tezos.get_contract_opt param.funds_recipient : unit contract option) with
        None -> (failwith "INVALID_SELLER" : operation)
        | Some contract -> Tezos.transaction () offer.price contract in

    let fee_transaction = match (Tezos.get_contract_opt store.config.proceed_recipient : unit contract option) with
        None -> (failwith "INVALID_PROCEED_RECIPIENT" : operation)
        | Some contract -> Tezos.transaction () offer.fee contract in

    let transfer_transaction = match (Tezos.get_entrypoint_opt "%transfer" param.token_contract : tzip12_transfer_param contract option) with
        None -> (failwith "INVALID_TOKEN_CONTRACT" : operation)
        | Some contract -> (
            let transfer_param: tzip12_transfer_param = [{ from_ = Tezos.get_sender(); txs = [{ to_ = param.buyer; token_id = param.token_id; amount = 1n }] }] in

            Tezos.transaction transfer_param 0mutez contract
        ) in

    ([payment_transaction; fee_transaction; transfer_transaction]), { store with offers = Big_map.remove key store.offers }


let update_config(param, store: config * storage) : return =
    let assert_zero_amount = require_zero_amount () in
    let assert_owner = if Tezos.get_sender() <> store.config.owner then (failwith "NOT_AUTHORIZED" : unit) in

    ([] : operation list), {store with config = param}

type parameter =
    Place_offer of place_offer_param
    | Remove_offer of remove_offer_param
    | Execute_offer of execute_offer_param
    | Update_config of config

let main (action, store : parameter * storage) : return =
    match action with
        | Place_offer p -> place_offer(p, store)
        | Execute_offer p -> execute_offer(p, store)
        | Remove_offer p -> remove_offer(p, store)
        | Update_config p -> update_config(p, store)
