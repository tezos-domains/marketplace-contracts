type place_offer_param = [@layout:comb] {
    token_contract: address;
    token_id: nat;
    price: tez;
    expiration: timestamp option;
}

type execute_offer_param = [@layout:comb] {
    token_contract: address;
    token_id: nat;
    seller: address;
}

type remove_offer_param = [@layout:comb] {
    token_contract: address;
    token_id: nat;
}