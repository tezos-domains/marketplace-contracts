#!/bin/sh
docker run --rm -v "$PWD":"$PWD" -w "$PWD" ligolang/ligo:0.49.0 "$@"