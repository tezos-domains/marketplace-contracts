import accounts from './sandbox-accounts';

const alice = accounts.alice;

function profiles(): Record<string, any> {
    return {
        default: {
            // used for local tests
            rpc: 'http://localhost:8732',
            ownerAddress: alice.pkh,
            secretKey: alice.sk,
        },
        florencenet: {
            rpc: 'https://florencenet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            deployed: require('../deployed/florencenet.json'),
        },
        granadanet: {
            rpc: 'https://granadanet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            deployed: require('../deployed/granadanet.json'),
        },
        hangzhounet: {
            rpc: 'https://hangzhounet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            deployed: require('../deployed/hangzhounet.json'),
        },
        ithacanet: {
            rpc: 'https://ithacanet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            deployed: require('../deployed/ithacanet.json'),
        },
        ghostnet: {
            rpc: 'https://ghostnet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            deployed: require('../deployed/ghostnet.json'),
        },
        jakartanet: {
            rpc: 'https://rpc.jakartanet.teztnets.xyz',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            deployed: require('../deployed/jakartanet.json'),
        },
        kathmandunet: {
            rpc: 'https://rpc.kathmandunet.teztnets.xyz',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            deployed: require('../deployed/kathmandunet.json'),
        },
        limanet: {
            rpc: 'https://rpc.limanet.teztnets.xyz',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            deployed: require('../deployed/limanet.json'),
        },
        mainnet: {
            rpc: 'https://mainnet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            deployed: require('../deployed/mainnet.json'),
            proceedRecipient: 'tz1PRDuo3HBGgT5VvJ876uoTtFrTbevTjoj7'
        },
    };
}

export function currentProfile(): any {
    const name = process.env.PROFILE || 'default';
    const profile = profiles()[name];
    if (!profile) {
        throw new Error(`No such profile: ${name}`);
    }
    return profile;
}
