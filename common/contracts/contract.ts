import { runOperation, tezosToolkit } from '../tezos-toolkit';
import { currentProfile } from '../profiles';
import { compileContract } from '../compile';

export abstract class Contract {
    protected abstract get name(): string;
    protected abstract get contractPath(): string;

    async deploy(config: any = {}, innerStorage: any = {}, statusReporter: (msg: string) => void = () => { }) {
        const tezos = tezosToolkit();
        const owner = currentProfile().ownerAddress;

        const storage = {
            ...(await this.buildInnerStorage(owner, config)),
            ...innerStorage,
        };

        statusReporter(`Originating ${this.name}`);

        const op = await runOperation(
            async () =>
                await tezos.contract.originate({
                    code: await compileContract(this.contractPath),
                    storage,
                    balance: config.balance,
                })
        );

        statusReporter(`Originated ${op.contractAddress}`);

        return await tezos.contract.at(op.contractAddress!);
    }

    protected abstract buildInnerStorage(owner: string, config: any): Promise<any>;
}
