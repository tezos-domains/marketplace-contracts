import { MichelsonMap } from '@taquito/michelson-encoder';
import { Contract } from './contract';

export class BuyOfferBrokerContract extends Contract {
    get name(): string {
        return 'BuyOfferBroker';
    }

    get contractPath(): string {
        return `src/contracts/buy-offer-broker/${this.name}.mligo`;
    }

    protected async buildInnerStorage(owner: string, config: any): Promise<any> {
        return {
            offers: new MichelsonMap(),
            config: {
                pause: false,
                fee_ratio: config.fee ? config.fee * 10000 : 25000,
                min_price: config.minPrice || 1000000,
                proceed_recipient: config.proceedRecipient || owner,
                owner,
            },
        };
    }
}

export const buyOfferBrokerContract = new BuyOfferBrokerContract();
