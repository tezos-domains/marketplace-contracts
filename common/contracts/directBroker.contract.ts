import { MichelsonMap } from '@taquito/michelson-encoder';
import { Contract } from './contract';

export class DirectBrokerContract extends Contract {
    get name(): string {
        return 'DirectBroker';
    }

    get contractPath(): string {
        return `src/contracts/direct-broker/${this.name}.mligo`;
    }

    protected async buildInnerStorage(owner: string, config: any): Promise<any> {
        return {
            offers: new MichelsonMap(),
            config: {
                pause: false,
                fee_ratio: config.fee ? config.fee * 10000 : 25000,
                min_price: config.minPrice || 1000000,
                proceed_recipient: config.proceedRecipient || owner,
                owner,
            },
        };
    }
}

export const directBrokerContract = new DirectBrokerContract();
