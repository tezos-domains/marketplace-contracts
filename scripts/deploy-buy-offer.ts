import { run } from '../common/utils';
import { buyOfferBrokerContract } from '../common/contracts/buyOfferBroker.contract';
import { currentProfile } from '../common/profiles';

run(async () => {
    const profile = currentProfile();

    await buyOfferBrokerContract.deploy(
        {
            fee: profile.fee,
            minPrice: profile.minPrice,
            proceedRecipient: profile.proceedRecipient,
        },
        {},
        console.log
    );
});
