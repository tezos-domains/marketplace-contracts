import { run } from '../../common/utils';
import { currentProfile } from '../../common/profiles';
import BigNumber from 'bignumber.js';

// PLAYGROUND for testing
run(async tezos => {
    const profile = currentProfile();
    const address = profile.deployed['DirectBroker'];

    const contract = await tezos.contract.at(address);

    const nameRegistry = await tezos.contract.at('KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p');

    //const op = await nameRegistry.methods.update_operators([{ add_operator: { owner: 'tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n', operator: address, token_id: 8252 } }]).send();

    const op = await contract.methods.place_offer('KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p', 8252, new BigNumber('150000000'), new Date(2022, 1, 1)).send();
    //const op = await contract.methods.remove_offer('KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p', 8252).send();

    // const op = await contract.methods.execute_offer('KT1PfBfkfUuvQRN8zuCAyp5MHjNrQqgevS9p', 8252, 'tz1SUrXU6cxioeyURSxTgaxmpSWgQq4PMSov').send({ 
    //     amount: 1000
    // });
    
    await op.confirmation();
});
