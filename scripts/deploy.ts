import { run } from '../common/utils';
import { directBrokerContract } from '../common/contracts/directBroker.contract';
import { currentProfile } from '../common/profiles';

run(async tezos => {
    const profile = currentProfile();

    await directBrokerContract.deploy(
        {
            fee: profile.fee,
            minPrice: profile.minPrice,
            proceedRecipient: profile.proceedRecipient,
        },
        {},
        console.log
    );
});
